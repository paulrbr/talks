#+TITLE: Ansible 101
#+REVEAL_ROOT: ./reveal.js
#+REVEAL_TRANS: fade
#+REVEAL_EXTRA_CSS: ./theme/trainlineeu.css
#+REVEAL_PLUGINS: (highlight notes)
#+REVEAL_HIGHLIGHT_CSS: ./theme/highlight-monokai-sublime.css
#+OPTIONS: reveal_control:nil reveal_slide_number:nil reveal_title_slide:nil
#+OPTIONS: toc:nil num:nil author:nil
#+OPTIONS: title:nil timestamp:nil
#+REVEAL_TITLE_SLIDE:
#+MACRO: NEWLINE @@latex:\\@@ @@html:<br>@@
# Local variables:
# eval: (add-hook 'after-save-hook 'org-reveal-export-to-html t t)
# end:

* Ansible 101

* Intro
   :PROPERTIES:
   :reveal_background: rgba(0,0,0,.2)
   :reveal_extra_attr: class="transparent-title"
   :END:

   #+BEGIN_QUOTE
   Ansible is an IT automation tool. It can configure systems, deploy software, and orchestrate more advanced IT tasks such as continuous deployments or zero downtime rolling updates.
   #+END_QUOTE
   @@html:<small>@@ [[https://docs.ansible.com/ansible/latest/index.html]] @@html:</small>@@

** Intro
   :PROPERTIES:
   :reveal_background: rgba(0,0,0,.2)
   :reveal_extra_attr: class="transparent-title"
   :END:

   #+BEGIN_QUOTE
   Ansible is an IT automation tool. It can *configure systems*, deploy software, and orchestrate more advanced IT tasks such as continuous deployments or zero downtime rolling updates.
   #+END_QUOTE
   @@html:<small>@@ https://docs.ansible.com/ansible/latest/index.html @@html:</small>@@

** Intro
   :PROPERTIES:
   :reveal_background: rgba(0,0,0,.2)
   :reveal_extra_attr: class="transparent-title"
   :END:

   #+BEGIN_QUOTE
   Ansible is an IT automation tool. It can configure systems, *deploy software*, and orchestrate more advanced IT tasks such as continuous deployments or zero downtime rolling updates.
   #+END_QUOTE
   @@html:<small>@@ https://docs.ansible.com/ansible/latest/index.html @@html:</small>@@

** Intro
   :PROPERTIES:
   :reveal_background: rgba(0,0,0,.2)
   :reveal_extra_attr: class="transparent-title"
   :END:

   #+BEGIN_QUOTE
   Ansible is an IT automation tool. It can configure systems, deploy software, and *orchestrate* more advanced *IT tasks* such as continuous deployments or zero downtime rolling updates.
   #+END_QUOTE
   @@html:<small>@@ https://docs.ansible.com/ansible/latest/index.html @@html:</small>@@

** "Push model"
   :PROPERTIES:
   :reveal_background: rgba(0,0,0,.2)
   :END:

   👩🏾 💻 *local*

   ⬇️ SSH

   🖥️ *Server*

** "Push model"
   :PROPERTIES:
   :reveal_background: rgba(0,0,0,.2)
   :END:

   👨🏿 💻 *local*

   ⬇️ SSH ⬇️ SSH ⬇️ SSH

   🖥️     🖥️     🖥️  *Servers*

** "Push model" via CI
   :PROPERTIES:
   :reveal_background: rgba(0,0,0,.2)
   :END:

   👩🏼 💻 *local*

   ⬇️ GIT

   🖥️ *Gitlab & Gitlab-CI*

   ⬇️ SSH ⬇️ SSH ⬇️ SSH

   🖥️     🖥️     🖥️  *Servers*

** DISCLAIMER
   :PROPERTIES:
   :reveal_background: rgba(0,0,0,.2)
   :END:

   ⚠ ~YAML~ all-the-things ⚠
   # file module documentation
   #+NAME: all_the_things
   #+ATTR_HTML: :width 15% :height 15% :class plain
   [[./images/all_the_things.jpg]]

* Vocabulary

** Vocabulary
#+ATTR_REVEAL: :frag (t t t t t)
+ ⨊  *modules* /provided by *Ansible*/ @@html:<small>@@(e.g. ~file~, ~copy~, ~apt~, ...)@@html:</small>@@
+ 📝 *Tasks* @@html:<small>@@(calling an Ansible module)@@html:</small>@@
+ 📦 *Roles* /defined by *community* or *us*/ @@html:<small>@@(A packaged set of tasks)@@html:</small>@@
+ 🖥️ *Inventories* /defined by *us*/ @@html:<small>@@(A list of servers and associated vars)@@html:</small>@@
  + @@html:<small>@@ ~hosts~ file @@html:</small>@@
  + @@html:<small>@@ ~group_vars/~ variables @@html:</small>@@
  + @@html:<small>@@ ~host_vars/~ variables @@html:</small>@@
+ 📦➡️🖥️ *Playbook* @@html:<small>@@(Applying a set of roles to an inventory)@@html:</small>@@

* Modules
  :PROPERTIES:
  :reveal_background: rgba(0,0,0,.2)
  :END:

@@html:<small>@@(e.g. ~file~, ~copy~, ~apt~, ...)@@html:</small>@@

** ~file~ module
  :PROPERTIES:
  :reveal_background: rgba(0,0,0,.2)
  :END:

   # file module documentation
   #+NAME: file_module_doc
   #+ATTR_HTML: :width 60% :height 60% :class plain
   [[./images/file_module_doc.png]]

** ~file~ module
  :PROPERTIES:
  :reveal_background: rgba(0,0,0,.2)
  :END:


   | Parameter                    | Choices/Defaults                                                                                                                            | Comments                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                     |
   |------------------------------+---------------------------------------------------------------------------------------------------------------------------------------------+--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
   | path @@html:</br>@@ *required* |                                                                                                                                             | path to the file being managed. Aliases: dest, name                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                          |
   | state                        | Choices: @@html:</br>@@ absent @@html:</br>@@ directory @@html:</br>@@ file ← @@html:</br>@@ hard  @@html:</br>@@ link @@html:</br>@@ touch | @@html:<small>@@ If directory, all intermediate subdirectories will be created if they do not exist. Since Ansible 1.7 they will be created with the supplied permissions. If file, the file will NOT be created if it does not exist; see the touch value or the copy or template module if you want that behavior. If link, the symbolic link will be created or changed. Use hard for hardlinks. If absent, directories will be recursively deleted, and files or symlinks will be unlinked. Note that absent will not cause file to fail if the path does not exist as the state did not change. If touch (new in 1.4), an empty file will be created if the path does not exist, while an existing file or directory will receive updated file access and modification times (similar to the way `touch` works from the command line).@@html:</small>@@ |
   | force @@html:</br>@@ /bool/    | Choices: @@html:</br>@@ no ← @@html:</br>@@ yes                                                                                             | ...                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                          |
   | group                        |                                                                                                                                             | ...                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                          |
   | mode                         |                                                                                                                                             | ...                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                          |
   | owner                        |                                                                                                                                             | ...                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                          |

** ~file~ module
  :PROPERTIES:
  :reveal_background: rgba(0,0,0,.2)
  :END:

   #+BEGIN_SRC yaml
     - name: Create nginx needed directories
       file:
         path: /etc/nginx/conf.d
         state: directory
   #+END_SRC

** Run ~file~ module
  :PROPERTIES:
  :reveal_background: rgba(0,0,0,.2)
  :END:

  👨🏻 💻 Run a single module with

   #+BEGIN_SRC shell
     > ansible -m file --args "state=touch path=/tmp/coucou" localhost
     localhost | SUCCESS => {
         "changed": true,
         "dest": "/tmp/coucou",
         "failed": false,
         "gid": 27,
         "group": "sudo",
         "mode": "0644",
         "owner": "paul",
         "size": 0,
         "state": "file",
         "uid": 1000
     }

     > ls -l /tmp/coucou
     -rw-r--r-- 1 paul sudo 0 Aug 27 14:48 /tmp/coucou

   #+END_SRC

** ~apt~ module
  :PROPERTIES:
  :reveal_background: rgba(0,0,0,.2)
  :END:

   #+BEGIN_SRC yaml
- name: Install nginx.
  apt:
    update_cache: yes
    cache_valid_time: 86400
    name: nginx
    state: present
   #+END_SRC

** 1850 modules available!
  :PROPERTIES:
  :reveal_background: rgba(0,0,0,.2)
  :END:

At the time of writing taken from
@@html:<small>@@ https://docs.ansible.com/ansible/latest/modules/ @@html:</small>@@

* Vocabulary
+ ⨊  *modules* /provided by *Ansible*/ @@html:<small>@@(e.g. ~file~, ~copy~, ~apt~, ...)@@html:</small>@@
+ 📝 *Tasks* @@html:<small>@@(calling an Ansible module)@@html:</small>@@
+ 📦 *Roles* /defined by *community* or *us*/ @@html:<small>@@(A packaged set of tasks)@@html:</small>@@
+ 🖥️ *Inventories* /defined by *us*/ @@html:<small>@@(A list of servers and associated vars)@@html:</small>@@
  + @@html:<small>@@ ~hosts~ file @@html:</small>@@
  + @@html:<small>@@ ~group_vars/~ variables @@html:</small>@@
  + @@html:<small>@@ ~host_vars/~ variables @@html:</small>@@
+ 📦➡️🖥️ *Playbook* @@html:<small>@@(Applying a set of roles to an inventory)@@html:</small>@@
* Roles

  @@html:<small>@@(A packaged set of tasks)@@html:</small>@@

** Community defined


   #+BEGIN_SRC shell
     vendor/
     ├── docker-gc
     ├── docker-ubuntu
     ├── geerlingguy.varnish
     ├── gitlab-runner
     └── rbenv

     5 directories, 0 files
   #+END_SRC

   👩🏼 💻 Installed with

   #+BEGIN_SRC shell
     > ansible-galaxy install --roles-path vendor/ -r requirements.yml
   #+END_SRC


** Defined by us

   #+BEGIN_SRC shell
     roles/
     ├── ...
     ├── common
     ├── docker
     ├── ...
     ├── local_setup
     ├── ...
     ├── nginx
     ├── ...
     └── slack_bot

     20 directories, 0 files
   #+END_SRC

** Role directory hierarchy

   #+BEGIN_SRC shell
     roles/nginx/
     ├── defaults/
     ├── vars/
     ├── handlers/
     ├── tasks/
     ├── files/
     └── templates/

     6 directories, 0 files
   #+END_SRC

** Role nginx tasks

   @@html:<small>@@ ~roles/nginx/tasks/nginx-app.yml~ file @@html:</small>@@
   #+BEGIN_SRC yaml
     - name: remove previous conf files
       file:
         path: "{{ item }}"
         state: absent
       with_fileglob:
         - /etc/nginx/sites-enabled/www.*.conf

     - name: Upload main CT nginx configuration
       template:
         src: www.conf.j2
         dest: "/etc/nginx/sites-enabled/www.{{ item.key }}.conf"
         with_dict: "{{ nginx_vhosts }}"
       notify: nginx reload
   #+END_SRC

** Role nginx handlers

   @@html:<small>@@ ~roles/nginx/handlers/all.yml~ file @@html:</small>@@
   #+BEGIN_SRC yaml
     - name: nginx reload
       service:
         name: nginx
         state: reloaded
   #+END_SRC

** Role nginx defaults

   @@html:<small>@@ ~roles/nginx/defaults/main.yml~ file @@html:</small>@@
   #+BEGIN_SRC yaml
     nginx_user: www-data
     nginx_vhosts: {}
     # ...
   #+END_SRC

** Role nginx templates

   @@html:<small>@@ ~roles/nginx/templates/www.conf.j2~ file @@html:</small>@@
   #+BEGIN_SRC nginx
     {% set server = item.value %}
     {% set vhost = item.key %}

     server {
       server_name {{ server.server_name }};
       listen {{ server.port | default(80) }};

       access_log /var/log/nginx/www-{{ vhost }}.log;
       error_log /var/log/nginx/www-{{ vhost }}-error.log;

       location / {
         proxy_pass  http://{{ server.proxy_destination }};
       }
     }
   #+END_SRC

* Vocabulary
+ ⨊  *modules* /provided by *Ansible*/ @@html:<small>@@(e.g. ~file~, ~copy~, ~apt~, ...)@@html:</small>@@
+ 📝 *Tasks* @@html:<small>@@(calling an Ansible module)@@html:</small>@@
+ 📦 *Roles* /defined by *community* or *us*/ @@html:<small>@@(A packaged set of tasks)@@html:</small>@@
+ 🖥️ *Inventories* /defined by *us*/ @@html:<small>@@(A list of servers and associated vars)@@html:</small>@@
  + @@html:<small>@@ ~hosts~ file @@html:</small>@@
  + @@html:<small>@@ ~group_vars/~ variables @@html:</small>@@
  + @@html:<small>@@ ~host_vars/~ variables @@html:</small>@@
+ 📦➡️🖥️ *Playbook* @@html:<small>@@(Applying a set of roles to an inventory)@@html:</small>@@
* Inventories
  :PROPERTIES:
  :reveal_background: rgba(0,0,0,.2)
  :END:

  @@html:<small>@@(A list of servers and associated vars)@@html:</small>@@

** Our inventories
  :PROPERTIES:
  :reveal_background: rgba(0,0,0,.2)
  :END:

   #+BEGIN_SRC shell
     ansible> find . -name hosts
     ./inventories/production/hosts
     ./inventories/integration/hosts
     ./inventories/infra/hosts
   #+END_SRC

** Inventory example
  :PROPERTIES:
  :reveal_background: rgba(0,0,0,.2)
  :END:

   #+BEGIN_SRC shell
     inventories/production/
     ├── group_vars/
     ├── host_vars/
     └── hosts

     2 directories, 1 file
   #+END_SRC

** Inventory example
  :PROPERTIES:
  :reveal_background: rgba(0,0,0,.2)
  :END:

  ~hosts~ file
  #+NAME: prod-inventory
  #+BEGIN_SRC ini
    [nginx]
    web[1:2].prod.example.org

    [app]
    sa[6:9].prod.example.org
  #+END_SRC

** Inventory variables
  :PROPERTIES:
  :reveal_background: rgba(0,0,0,.2)
  :END:

  @@html:<small>@@ ~group_vars/nginx/vars.yml~ file @@html:</small>@@
  #+NAME: prod-inventory
  #+BEGIN_SRC yaml
    nginx_vhosts:
      www.example.org:
        port: 8080
        server_name: "www.example.org example.org"
        proxy_destination: "localhost:5000"
  #+END_SRC

  @@html:<small>@@ ~host_vars/web1.prod.example.org/vars.yml~ file @@html:</small>@@
  #+NAME: prod-inventory
  #+BEGIN_SRC yaml
    private_ip: 172.16.10.1
  #+END_SRC

** Inventory (secret) variables
   :PROPERTIES:
   :reveal_background: rgba(0,0,0,.2)
   :END:

  @@html:<small>@@ ~group_vars/nginx/vault.yml~ file @@html:</small>@@
  #+BEGIN_SRC plain
$ANSIBLE_VAULT;1.1;AES256
33326630376138613334303363306434623065383236366162616132383566386662636663666563
6133333065326439306366313532633561663266353665350a393932366364623236313330653734
34383239373434653835633131366636343736633766316532613230336132326138636165346461
3365656562336633360a636235343166366537383861643136336435343865383233376262613139
37626163316532636266636662373335343764626562376437653535303562633164313836376565
35613861346538303837653438383535663563643138313333333962383962326632323838386431
65646633646435396630623462626665326262393538613939396265333435366431633031323535
33353232656437356663
  #+END_SRC

** Inventory (secret) variables
   :PROPERTIES:
   :reveal_background: rgba(0,0,0,.2)
   :END:

  👨🏻 💻 Edit vaulted variable file with

  #+BEGIN_SRC shell
    > ansible-vault edit production/group_vars/nginx/vault.yml
    Vault password: *********
  #+END_SRC


  @@html:<small>@@ ~group_vars/nginx/vault.yml~ file (decrypted) @@html:</small>@@
  #+BEGIN_SRC yaml
    nginx_secret: MySuperSecretValue
    ssl_cert: |
       -----BEGIN CERTIFICATE-----
       JLlkjjj2qjj...
  #+END_SRC

* Vocabulary
+ ⨊  *modules* /provided by *Ansible*/ @@html:<small>@@(e.g. ~file~, ~copy~, ~apt~, ...)@@html:</small>@@
+ 📝 *Tasks* @@html:<small>@@(calling an Ansible module)@@html:</small>@@
+ 📦 *Roles* /defined by *community* or *us*/ @@html:<small>@@(A packaged set of tasks)@@html:</small>@@
+ 🖥️ *Inventories* /defined by *us*/ @@html:<small>@@(A list of servers and associated vars)@@html:</small>@@
  + @@html:<small>@@ ~hosts~ file @@html:</small>@@
  + @@html:<small>@@ ~group_vars/~ variables @@html:</small>@@
  + @@html:<small>@@ ~host_vars/~ variables @@html:</small>@@
+ 📦➡️🖥️ *Playbook* @@html:<small>@@(Applying a set of roles to an inventory)@@html:</small>@@
* Playbooks

@@html:<small>@@(Applying a set of roles to an inventory)@@html:</small>@@

** Our playbooks

   #+BEGIN_SRC sh
     > git ls-files *.yml
     # Deploy Software
     clever_deploy.yml

     # Configure local system
     local.yml
     # Configure remote systems
     app.yml
     webservers.yml

     # Orchestrate
     restart.yml
     upgrade.yml
   #+END_SRC

** ~setup.yml~

   #+BEGIN_SRC yaml
     # Ability to include other playbook
     - include: notify_begin.yml
       when: app_name is defined

     - hosts: all
       serial: [ "34%", "34%", "100%" ]
       roles:
         - role: common
         # ...
         - role: nginx
           when: "nginx_setup | default(false)"
           tags: ['nginx']
         # ...
         - role: redis
           when: "'redis' in group_names"
           tags: ['redis']
         - role: ruby
           deploy_step: 'prepare'
           when: rbenv is defined
           tags: [ 'ruby' ]
         # ...

     - include: notify_end.yml
       when: app_name is defined
   #+END_SRC

** Running a playbook

   #+BEGIN_SRC shell
     > ansible-playbook \
       --inventory production \
       --limit web1.prod.example.org \
       --tag nginx \
       setup.yml
   #+END_SRC

* Vocabulary
+ ⨊  *modules* /provided by *Ansible*/ @@html:<small>@@(e.g. ~file~, ~copy~, ~apt~, ...)@@html:</small>@@
+ 📝 *Tasks* @@html:<small>@@(calling an Ansible module)@@html:</small>@@
+ 📦 *Roles* /defined by *community* or *us*/ @@html:<small>@@(A packaged set of tasks)@@html:</small>@@
+ 🖥️ *Inventories* /defined by *us*/ @@html:<small>@@(A list of servers and associated vars)@@html:</small>@@
  + @@html:<small>@@ ~hosts~ file @@html:</small>@@
  + @@html:<small>@@ ~group_vars/~ variables @@html:</small>@@
  + @@html:<small>@@ ~host_vars/~ variables @@html:</small>@@
+ 📦➡️🖥️ *Playbook* @@html:<small>@@(Applying a set of roles to an inventory)@@html:</small>@@
* Help!
   :PROPERTIES:
   :reveal_background: rgba(0,0,0,.2)
   :END:

   @@html:<small>@@ Forgot which binary to use?  @@html:</small>@@


** Ansible makefile helper
   :PROPERTIES:
   :reveal_background: rgba(0,0,0,.2)
   :END:

   #+BEGIN_SRC shell
     > make
     app-specific-env       make app-specific-env app_name=<app> output_dir=/tmp env=integration limit=dev
     console                make console # Run an ansible console
     debug                  make debug host=myhost # Debug a host's variable
     deploy                 make deploy app_name=<app> env=integration limit=dev # Launch an app deploy
     dry-run                make dry-run [playbook=setup] [env=integration] [tag=<ansible-tag>] [limit=<ansible-host-limit>] [args=<ansiblearguments>] # Run a playbook in dry run mode
     facts                  make facts group=all # Gather facts from your hosts
     install                make install # Install roles dependencies
     inventory-report       make inventory-report #
     lint                   make lint playbook=setup # Check syntax of a playbook
     list                   make list # List hosts inventory
     run                    make run [playbook=setup] [env=integration] [tag=<ansible-tag>] [limit=<ansible-host-limit>] [args=<ansiblearguments>] # Run a playbook
     vault                  make vault file=/tmp/vault.yml # Edit or create a vaulted file
   #+END_SRC

   @@html:<small>@@ 💻 [[https://github.com/paulrbr/ansible-makefile][paulrbr/ansible-makefile]] @@html:</small>@@

* Thanks
 :PROPERTIES:
 :reveal_extra_attr: class="transparent-title"
 :END:

 *Questions?*
 #+ATTR_REVEAL: :frag t
 --- /Thanks!/
 #+ATTR_REVEAL: :frag (t)
 - Follow me *[[https://mastodon.social/@paulrbr][@paulrbr@mastodon.social]]*
 - Slides *https://paulrbr.gitlab.io/talks/ansible-101.html*

