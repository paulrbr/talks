;;; publish.el --- Publish reveal.js presentation from Org files on Gitlab Pages
;; -*- Mode: Emacs-Lisp -*-
;; -*- coding: utf-8 -*-

;; Copyright (C) 2017 Jens Lechtenbörger
;; Copyright (C) 2019 Edited by Paul Bonaud

;;; License: GPLv3

;;; Commentary:
;; Inspired by publish.el by Jens:
;; https://gitlab.com/pages/emacs-reveal/raw/master/elisp/publish.el

;;; Code:
(package-initialize)
(add-to-list 'package-archives '("org" . "https://orgmode.org/elpa/") t)
(add-to-list 'package-archives '("melpa" . "https://melpa.org/packages/") t)
(package-refresh-contents)
(package-install 'org-re-reveal)

(require 'org)
(require 'ox-publish)
(require 'org-re-reveal)

(defun org-reveal-publish-to-reveal
    (plist filename pub-dir)
  "Publish an org file to Html.
FILENAME is the filename of the Org file to be published.  PLIST
is the property list for the given project.  PUB-DIR is the
publishing directory.
Return output file name."
  (org-publish-org-to 're-reveal filename ".html" plist pub-dir))

(setq org-export-with-smart-quotes t
      org-confirm-babel-evaluate nil)

(setq org-publish-project-alist
      (list
       (list "slides"
             :base-directory "."
             :base-extension "org"
             :publishing-function 'org-reveal-publish-to-reveal
             :publishing-directory "./")))

(provide 'publish)
;;; publish.el ends here
